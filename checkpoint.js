    ////////////////
    // exercise 1 //
    ////////////////


function categorize(array) {
    const obj = {}
    array.map((element) => {
       if (obj[typeof element] === undefined) {
           obj[typeof element] = []
       }  // rempli l'objet avec toutes les clés qui correspondent aux types des éléments du tableau (si elle n'existe pas déjà), et leur attribue un tableau vide comme valeur
    })
    array.map((element) => {
       obj[typeof element].push(element)
    })  // rempli les tableaux vides de l'objet avec les éléments du tableau
    return obj
}

const array = [1, 'hello', function sayHi(){ console.log('hi') }, 'world', true, 0n,
1000]

console.log("--- Exercise 1 : ---")
console.log(categorize(array))




    ////////////////
    // exercise 2 // 
    ////////////////


Array.prototype.dedup = function() {
    // la fonction filter créé  un nouveau tableau avec tous les éléments du tableau courant qui passent le test.
    return array.filter((element, index) => array.indexOf(element) === index)
        // array.indexOf(element) === index est le test qui vérifie si l'index de la première occurence de l'élément est égal à l'index de l'élément courant.
}

const array2 = [1, "hello", "hello", "world", true, 1, 1000, "hello", "world", true, 0n, 1000, "hello"]

console.log("--- Exercise 2 : ---")
console.log(array2.dedup())




    ////////////////
    // exercise 3 //
    ////////////////


const obj = {
    foo: 1,
    bar: 'hello',
    baz: true
    }

const filtered = filterObject(obj, (key, value) => key === 'foo' || value === 'hello')
    
function filterObject(obj, predicat) {
    const result = {}
    for (let key in obj) {
        let value = obj[key]
        if (!predicat(key, value)) { // si le predicat est faux, ajout de la clé/valeur à l'objet result
            result[key] = value 
        }
    }
    return result
}

console.log("--- Exercise 3 : ---")
console.log(filtered)




    ////////////////
    // exercise 4 //
    ////////////////


const asyncJob = (n) => Math.random() > 0.5 ? Promise.resolve(n + 1) :
Promise.reject(Error('boom'))

async function jobAsync(){
    try {
    // dans l'exemple, on fait un asyncJob(0) puis un asyncJob du résultat, puis 3 asyncJob du résultat2, puis un asyncJob de la somme des 3 résultats

     // on commence par l'asyncJob(0)
     const job1 = await asyncJob(0)
     // on fait un asyncJob du résultat
     const job2 = await asyncJob(job1)
     // on fait 3 asyncJob du résultat2
     const [job3, job4, job5] = await Promise.all([
         asyncJob(job2),
         asyncJob(job2),
         asyncJob(job2)
     ])
     // on fait un asyncJob de la somme des 3 résultats
     const jobFinal = await asyncJob(job3 + job4 + job5)
     console.log("résultat final : "+  jobFinal)
    }
    catch (err) {
        console.error("gestion erreur globale: " + err.message);
    }
}

console.log("--- Exercise 4 : ---")
jobAsync()




    ////////////////
    // exercise 5 //
    ////////////////


const p1 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 5000, "1st promise resolved");
    });
const p2 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 1000, "2nd promise resolved");
    });
const p3 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 3000, "3rd promise resolved");
    });
const p4 = new Promise(function (resolve, reject) {
    setTimeout(reject, 2000, "4th promise rejected");
    });
const p5 = new Promise(function (resolve, reject) {
    setTimeout(reject, 200, "5th promise rejected");
    });

function race(promises){
return new Promise((resolve, reject) => {
    for (let promise of promises) {
        promise
            .then(value => resolve(value))
            .catch(err => reject(err))
    }
})
}

console.log("--- Exercise 5 : ---");
race([p1, p2, p3, p4, p5])
    .then(value => console.log("result ! " + value))
    .catch(err => console.log("catch ! " + err));




    ////////////////
    // exercise 6 //
    ////////////////


const p6 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 5000, "6th promise resolved");
    });
const p7 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 1000, "7th promise resolved");
    });
const p8 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 4000, "8th promise resolved");
    });
const p9 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 3000, "9th promise resolved");
    });

function promiseAll(promises) {
    const result = []
    return new Promise((resolve, reject) => {
        for (let promise of promises) {
            promise
                .then(value => {
                    result.push(" " + value)
                    if (result.length === promises.length) {
                        resolve(result)
                    }
                })
                .catch(err => reject(err))
        }
    }
    )
}

console.log("--- Exercise 6 : ---");
promiseAll([p6, p7, p8, p9])
        .then(value => console.log("result ! "+value))
        .catch(err => console.log("catch ! "+err))